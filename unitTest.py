import os

def checkBankCSVandZip(csvName, zipName):
    try:
        bank_csv = os.path.isfile(csvName)
        bank_zip = os.path.isfile(zipName)

        if (bank_csv):
            return True
        else:
            return False

    except ImportError:
        print("Couldn't find these downloaded file")



def checkHistogramDir(directory):

    try:
        histogram_dir = os.path.exists(directory)
        if (histogram_dir == True):
            return True
        else:
            return False

    except ImportError:
        print("Couldn't find these downloaded file")



def checkPlotFiles(plotBox, plotDot):
    try:
        box_plot = os.path.isfile(plotBox)
        dot_plot = os.path.isfile(plotDot)
        if (dot_plot == True and box_plot == True):
            return True
        else:
            return False

    except ImportError:
        print("Couldn't find the dot file and box file")




def checkBarandPieDiagram(barDiagram, pieChart):
    try:
        barDiagram = os.path.isfile(barDiagram)
        pieChart = os.path.isfile(pieChart)
        if (barDiagram == True and pieChart == True):
            return True
        else:
            return False

    except ImportError:
        print("Couldn't find the barDiagram file and pieChart html file")


def checkProcessData(processedData):
    try:
        barDiagram = os.path.isfile(processedData)

        if (barDiagram == True):
            return True
        else:
            return False

    except ImportError:
        print("Couldn't find the barDiagram file and pieChart html file")




