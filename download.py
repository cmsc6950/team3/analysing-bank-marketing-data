#!/usr/bin/env python
# coding: utf-8
# Usage : python3 download.py -u "http://archive.ics.uci.edu/ml/machine-learning-databases/00222/bank.zip" -zf "bank.zip" -of "bank.csv"
# This script downloads the data on the directory/folder from where you run this code(in current directory)

import requests
from zipfile import ZipFile
import argparse
import os


# function: download files
def download():
    # getting arguments from users

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("-u", "--url", required=True, type=str)
        parser.add_argument("-zf", "--zipfile", required=True, type=str)
        parser.add_argument("-of", "--outfile", required=True, type=str)
        # parse arguments
        args = vars(parser.parse_args())

        # check arguments exists in this sequence
        sequence = ["url", "zipfile", "outfile"]
        if (checkArgumentExists(args, sequence)):

            req = requests.get(args["url"])
            file = open(args["zipfile"], 'wb')
            for item in req.iter_content(chunk_size=1024):
                file.write(item)
            file.close()
            bank_zip = ZipFile(args["zipfile"])
            bank_zip.extract(args["outfile"])
            bank_zip.close()


    except ImportError:
        print("No argument passed!")


# check the arguments and matched with the sequence
# sequence = "-u" | zipfile = "-zf" | outfile = "-of"
def checkArgumentExists(arguments, sequence):
    count = 0
    try:
        flag = True
        for key, value in arguments.items():
            if (key != sequence[count] and value == ''):
                flag = False
            count += 1

        return flag


    except ValueError:
        print("Too Many arguments passed!")

    except TypeError:
        print("The input is not a sequence as given!")

    except KeyError as kError:
        print("The input is not witin standard way!", kError)


# calling the download file
download()