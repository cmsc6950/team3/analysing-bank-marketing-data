# Analysing Bank Marketing Data #
------
<details>
  <summary>Contents</summary>
    <p> 
      
    1. Introduction  
    2. Data Specifications
        2.1 Original Data
        2.2 Processed Data
        2.3 Feature Information
    3. Steps to build the project
        3.1 Download Data
        3.2 Plot Histograms
        3.3 Clean Data
        3.4 Univariate Plots
            3.4.1 Dot Plot
            3.4.2 Box Plot
        3.5 Multivariate Plots
            3.5.1 PieChart
            3.5.2 BarDiagram
        3.6 Unit Testing
        3.7 Virtual Env
        3.8 Makefile
    4  Documentation
        4.1 LaTeX Project Report
        4.2 Web Based Presentation
    5. Contributors
        
   </p>
</details>

## 1. Introduction

    The goal is to analyze the bank marketing data to find out the impact of age, job, loan, duration and marital status, etc on the subscription of a bank product (term deposit) by customers.

    The dataset used in this project is based on "Bank Marketing" UCI dataset. The data is associated with direct marketing campaigns of a Portuguese banking organization from May 2008 to November 2010. The campaigns were driven through phone calls.
    
    Data Source link - http://archive.ics.uci.edu/ml/datasets/Bank+Marketing

## 2. Data Specifications

    Data file name: bank.csv
    
#### 2.1 Original Data
    
    Number of Instances: 4521   
    Number of Features: 17

#### 2.2 Processed Data

    Number of Instances: 4500  
    Number of Features: 7
    
#### 2.3 Feature Information

    7 out of 17 attributes are being used in this project.

    | # | Original Feature label | New feature label                 |
    |---|------------------------|-----------------------------------|
    | 1 | age                    | Age                               |
    | 2 | job                    | Job                               |
    | 3 | marital                | Marital Status                    |
    | 4 | balance                | Avg yearly balance                |
    | 5 | housing                | Housing Loan                      |
    | 6 | campaign               | # of times customer was contacted |
    | 7 | y                      | Term deposit subscription         |


    
## 3. Steps to build the project

#### 3.1 Download Data

    The first step is to download the required data. The file 'download.py' is for automatically downloading data from the UCI repository using the requests module. It takes three command line arguments url, zipfile to be downloaded and the particular file to be extracted from the zipfile. It fetches and downloads the 'bank.csv' file as well as the bank.zip file in the current folder. Refer to the usage below to run 'download.py' script from the command line.

    Usage : python3 download.py -u "http://archive.ics.uci.edu/ml/machine-learning-databases/00222/bank.zip" -zf "bank.zip" -of "bank.csv"

#### 3.2 Plot Histograms

    The next step is to have some visualization of the data. Here, the histograms of all the numerical attributes in the raw data are plotted in order to understand the type of data we have. The file 'histogram.py' can be run to get the histogram plots of each numerical attribute. Refer to the usage below to run 'download.py' script from the command line.
    
    Usage : python3 histogram.py
    
#### 3.3 Clean Data

    In order for data to be more clear and useful, the data is processed and cleaned in this step. The file 'cleandata.py' is to get the clean version from the raw data. Running this script generates the 'ProcessedBankData.csv' file in the current folder and takes the 'bank-full.csv' file as the input obtained from the download data step from the current folder. Refer to the usage below to run 'cleandata.py' script from the command line.
    
    Usage : python3 cleandata.py
    
#### 3.4. Univariate Plots 
    Univariate plots are useful to understand the distribution of each attribute. We used two different types of univariate plots, dot plot also known as strip plot and box plot. 
    
##### 3.4.1 Dot Plot
    Dot plot shows changes between the two conditions. Dot plots show a relation between various attributes from Bank database to that of Term deposit. The file 'Plot_Dot.py' is for plotting the dot plots attributes, each against Term deposit subscription. Refer to the usage below to run the script from the command line.
    
    Usage : python3 Plot_Dot.py
    
##### 3.4.2 Box Plot 
    Box plot makes a box and a whisker plot for each column of an attribute. The file 'Plot_Box.py' is for plotting the box plots for different attributes. Refer to the usage below to run the script from the command line.
    
    Usage : python3 Plot_Box.py
    

#### 3.5. Multivariate Plots 
    Multivariate plots are useful to understand the relationship among attributes. We used two different types of multivariate plots, pie chart also and the bar diagram.
    
##### 3.5.1 PieChart - Showing the relationships between Age interval, house loan, and Term Deposit subscription
    In the multivariate plots of the pie chart, we used the following attributes; Age(used age interval), Housing loan, and Term deposit subscription). PieChart was plotted to find the relationship between these attributes. Refer to the usage below to run 'pieChart.py' script from the command line. 
    
    Usage : python3 pieChart.py
    Output:    

![Screen Shot 2019-06-13 at 1](https://user-images.githubusercontent.com/13005159/59450221-508cad00-8de3-11e9-97b3-aa92aeb83f4a.png)

      
##### 3.5.2 BarDiagram - Understanding the relationships between Age interval, job, and Avg yearly balance    
    In the multivariate plots of the Bar Diagram, we used the following attributes; Age(used age interval), job, and Avg yearly balance. BarDiagram was plotted to find the relationship between these attributes. Refer to the usage below to run 'barDiagram.py' script from the command line. 
    
    Usage : python3 barDiagram.py
    Output:       

![Screen Shot 2019-06-13 at 3](https://user-images.githubusercontent.com/13005159/59454516-d52ff900-8dec-11e9-8c83-4d0d30adb916.png)

#### 3.6 Unit Testing
    In the UnitTest.py file, we test the components of the application.
    checkBankCSVandZip() and checkProcessData() functions check if data files were gererated from download.py and cleandata.py.
    checkHistogramDir() function checks if the histogram directory exists and the generated histogram plots from histogram.py.
    checkPlotFiles() checks both box plot and dot plots.
    checkBarandPieDiagram() function checks if the pieDiagram.py and barDiagram.py generated two html files.

#### 3.7 Virtual Env
    Pipenv is a production-ready tool that aims to bring the best of all packaging worlds to the Python world. We used pipenv lock to compile the dependencies on your development environment and deploy the compiled Pipfile.lock to all of your production environments for reproducible builds. 

#### 3.8 Makefile
    Makefile is a simple way to organize the codes and to make a project reproducible easily. It has the targets and dependencies. The file name is Makefile. To reproduce this project, refer to one of the two below commands.
    
    Usage: 'make' or 'make -f <makefile>'
    
    Run the following command to clean the already existing targets.
    
    Usage: make clean

## 4. Documentation
    All the details of this project have been documented in the LaTeX project report and also a briefing in a web-based presentation.

#### 4.1 LaTeX Project Report
    The report for this project was created using Latex. The report details the entire project step by step including the dataset and the approach. It also summarizes the results. The file 'CMSC6950ProjectReport.tex' which generates the 'CMSC6950ProjectReport.pdf'. Running Makefile also generates the CMSC6950ProjectReport.pdf file.

#### 4.2 Web Based Presentation
    The web-based presentation was created using the remark-js library. The presentation has a brief overview of the project and the contribution of each team member to the project. Web based presentation file is ''.
    
## 5. Contributors
    Kratika Naskulwar
    Praveena Pinnika
    Jewel Mahmud Nimul Shamim
    Bhumi Tailor
