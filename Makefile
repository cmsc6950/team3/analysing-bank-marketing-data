# Makefile to process datafiles, generate a plot and build LaTeX report.

DATA = bank.csv
PROCESSED = ProcessedBankData.csv

CMSC6950ProjectReport.pdf: CMSC6950ProjectReport.tex unittesttarget
	latexmk -pdf -pdflatex='pdflatex -shell-escape'

unittesttarget: test_unit.py bartarget
	py.test $<

bartarget: barDiagram.py jittertarget
	python3 $<

jittertarget: morphology.py pietarget
	python3 $<

pietarget: pieChart.py boxplottarget
	python3 $< 
	
boxplottarget: Plot_Box.py dotplottarget
	python3 $<

dotplottarget: Plot_Dot.py $(PROCESSED)
	python3 $<

$(PROCESSED): cleandata.py histogramtarget
	python3 $< 

histogramtarget: histogram.py $(DATA)
	python3 $< 

$(DATA): download.py
	python3 $< -u "http://archive.ics.uci.edu/ml/machine-learning-databases/00222/bank.zip" -zf "bank.zip" -of "bank.csv"

.PHONY:  clean 

clean:
	latexmk -c
	rm -f CMSC6950ProjectReport.pdf
	rm -f $(PROCESSED)
	rm -f $(DATA)
	rm -f bank.zip
	rm -f *.png
	rm -r histograms
	rm -f *.html


