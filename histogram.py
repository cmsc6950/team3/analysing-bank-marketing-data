import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

def histogram(path):
    if(os.path.exists("histograms")):
        pass
    else:
        os.mkdir("histograms")


    data=pd.read_csv(path,sep=';', header=0)
    #data.head()
    #age
    age=data['age']
    plt.hist(age, bins=[10,20,30,40,50,60,70,80,90,100],rwidth=0.95)
    plt.ylabel('Number of Customers');
    plt.xlabel('Age')
    plt.savefig('histograms/age.png')
    plt.show()

   #Banlance
    balance=data['balance']
    plt.hist(balance,bins=30,rwidth=0.95)
    plt.ylabel('Number of Customers');
    plt.xlabel('Balance')
    plt.savefig('histograms/balance.png')
    plt.show()

    # Contact Duration
    duration=data['duration']
    plt.hist(duration, bins=[500,1000,1500,2000,2500,3000,3500,4000,4500,5000,6000],rwidth=0.95)
    plt.ylabel('Number of Customers');
    plt.xlabel('last contact call duration in seconds')
    plt.savefig('histograms/duration.png')
    plt.show()

    #previous: number of contacts performed
    #before this campaign and for this client (numeric)
    previous=data['previous']
    plt.hist(previous, bins=[0,2,4,6,8,10,12,14],rwidth=0.95)
    plt.ylabel('Number of Customers');
    plt.xlabel('number of contacts before this campaign')
    plt.savefig('histograms/previous.png')
    plt.show()


    # campaign: number of contacts performed during this campaign
    campaign=data['campaign']
    plt.hist(campaign, bins=[0,1,2,3,4,5,6,7,8,9,10],rwidth=0.95)
    plt.ylabel('Number of Customers');
    plt.xlabel('number of contacts performed during this campaign')
    plt.savefig('histograms/campaign.png')
    plt.show()
    
histogram("bank.csv")






