from unitTest import checkBankCSVandZip
from unitTest import checkHistogramDir
from unitTest import checkPlotFiles
from unitTest import checkBarandPieDiagram
from unitTest import checkProcessData

def test_csv_and_zip_file_exist():
    observed = checkBankCSVandZip("bank.csv", "bank.zip")
    expected = True
    assert (observed == expected), "bank.csv and bank.zip are not downloaded"


def test_processed_data():
    observed = checkProcessData("ProcessedBankData.csv")
    expected = True

    assert (observed == expected), "Processed CSV file couldn't found"


def test_check_histogram_dir():
    observed = checkHistogramDir("histograms")
    expected = True

    assert (observed == expected), "Histogram is not created!"



def test_plotfiles():
    observed = checkPlotFiles("Plot_Box.png", "Plot_Box.png")
    expected = True

    assert (observed == expected), "File box plot and dot plot are not found!"


def test_diagrams():
    observed = checkBarandPieDiagram("barDiagram.html", "pieChart.html")
    expected = True

    assert (observed == expected), "HTML File barDiagram and pieChart are not found!"
