import math

from bokeh.io import show, output_file
from bokeh.models import ColumnDataSource
from bokeh.palettes import Spectral6
from bokeh.plotting import figure
from bokeh.transform import factor_cmap

import cleandata
import pandas as pd


def barDiagram():

    cleandata.cleandata()
    bankdf = pd.read_csv('ProcessedBankData.csv', sep=',', header=0)
    # bankdf = bankdf.head(500)


    # getting the user age unique
    print(bankdf['Age'].unique().tolist())
    unique_age_people = bankdf['Age'].unique().tolist();
    print(unique_age_people.sort())


    output_file("barDiagram.html")

    jobs = {
        'admin.': 1,
        'blue-collar': 2,
        'entrepreneur': 3,
        'housemaid': 4,
        'management':5,
        'retired': 6,
        'self-employed': 7,
        'services': 8,
        'student': 9,
        'technician': 10,
        'unemployed': 11,
        'unknown': 12
    }

    range_first = 15
    range_last = 20
    counts = []
    for key, value in jobs.items():
        total_average_salary = 0
        count = 0
        total_people = 0
        if (range_first < 100 and range_last < 100):
            for index, row in bankdf.iterrows():
                if row['Age'] >= range_first and row['Age'] < range_last and value == row['Job']:
                    total_average_salary += row['Avg yearly balance']
                    total_people += 1

            if (total_average_salary == 0):
                average_yearly_salary = 0
            else:
                average_yearly_salary = total_average_salary / total_people
            range_first += 5
            range_last += 5




        counts.append(average_yearly_salary)


    print(counts)





    fruits = ['Admin', 'blue-collar', 'entrepreneur', 'House maid', 'management', 'retired', 'self-employed', 'services', 'student', 'technician', 'unemployed', 'unknown']


    source = ColumnDataSource(data=dict(fruits=fruits, counts=counts))

    p = figure(x_range=fruits, plot_height=400, plot_width=600, toolbar_location=None, title="Relationship Between Age Intervals, Jobs, and Average Yearly Balance")
    p.vbar(x='fruits', top='counts', width=0.9, source=source, legend=False,
           line_color='white', fill_color=factor_cmap('fruits', palette=Spectral6, factors=fruits))

    p.xaxis.axis_label = 'Jobs'
    p.yaxis.axis_label = 'Average Yearly Balance'


    p.xaxis.major_label_orientation = math.pi / 3
    p.xgrid.grid_line_color = None
    p.y_range.start = 0
    p.y_range.end = max(counts) + 200
    p.legend.orientation = "horizontal"
    p.legend.location = "top_center"

    show(p)


barDiagram()