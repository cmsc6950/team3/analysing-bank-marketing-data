import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import style
import matplotlib.gridspec as gridspec
# x = []
# y = []
plots = pd.read_csv("ProcessedBankData.csv")
#print(plots.head())
#plt.boxplot(plots["Age"])



fig, ax = plt.subplots(2,2)  # a figure with a 1x2 grid of Axes
fig.suptitle('Box plots')

ax[0,0].boxplot(plots["Age"])
ax[0,0].set_title('Age')

# ax[0,0].set_xlabel('Age')
ax[0,1].boxplot(plots["Job"])
# ax[0,1].set_xlabel('Job')
ax[0,1].set_title('Job')
ax[1,0].boxplot(plots["Avg yearly balance"])
ax[1,0].set_xlabel('Avg yearly balance')
ax[1,0].set_ylim(-4000,10000)
ax[1,1].boxplot(plots["# of times customer was contacted"])
ax[1,1].set_xlabel('No of times customer was contacted')
ax[1,1].set_ylim(0,35)
fig.align_ylabels(ax)

plt.boxplot(plots["Job"])

# plt.boxplot(plots["Term deposit subscription"],plots["Age"])
# plt.xlabel('Age')
# plt.xlabel('Term deposit')
plt.show()
fig.savefig('Plot_Box.png')