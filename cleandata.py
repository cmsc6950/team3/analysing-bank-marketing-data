#!/usr/bin/env python
# coding: utf-8
# Usage : python3 cleandata.py
# This function cleans the data and writes it to a file called "ProcessedBankData.csv " and keeps this file in current directory
 #read csv - This file "bank-full.csv" should be in the current directory
import pandas as pd
import numpy  as np 

def cleandata():
    #Read csv file "bank.csv" from current folder
    bankdf = pd.read_csv('bank.csv', sep=';', header=0)
    print("Shape of Original data", bankdf.shape)
    
    #Drop extra columns
    bankdf1 = bankdf.drop(['default',
                           'loan',
                           'education',
                           'contact',
                           'day',
                           'month',
                           'duration',
                           'pdays',
                           'previous',
                           'poutcome'], axis=1)
    print("Data shape after dropping extra columns", bankdf1.shape)
    
    #Change string/object values from columns to integers
    #job , marital, default, housing, loan, y
    newjob = {'admin.': 1,
              'blue-collar': 2,
              'entrepreneur': 3,
              'housemaid': 4,
              'management':5,
              'retired': 6,
              'self-employed': 7,
              'services': 8,
              'student': 9,
              'technician': 10,
              'unemployed': 11,
              'unknown': 12} 
    bankdf1.job = [newjob[item] for item in bankdf.job]

    newms = {'married': 1,'single': 2,'divorced': 3} 
    bankdf1.marital = [newms[item] for item in bankdf1.marital]

    newhl = {'yes': 1,'no': 0} 
    bankdf1.housing = [newhl[item] for item in bankdf1.housing]

    newy = {'yes': 1,'no': 0} 
    bankdf1.y = [newy[item] for item in bankdf1.y]

    #Remove duplicate rows from data with reduced columns(now columns are 9)
    bankdf2 = bankdf1.drop_duplicates()
    print("Data shape after dropping duplicate rows across 9 columns", bankdf2.shape)
    
    #yet to change index
    #bankdf2.set_index('age')
    
    #Rename column labels
    bankdf2.rename(columns={'age':'Age',
                            'job':'Job',
                            'marital':'Marital status',
                            'balance':'Avg yearly balance',
                            'housing':'Housing loan',
                            'campaign':'# of times customer was contacted',
                            'y':'Term deposit subscription'}, inplace=True)

    missing = bankdf2.isnull().sum().sum()
    print("Number of missing values across rows and columns",missing)  
    
    print(bankdf2.head(3))
    bankdf2.to_csv('ProcessedBankData.csv',index=False)
    
cleandata()





