from bokeh.plotting import figure
from bokeh.plotting import show
from bokeh.plotting import output_file
from bokeh.sampledata.iris import flowers

import pandas as pd
import cleandata


cleandata.cleandata()
bankdf = pd.read_csv('ProcessedBankData.csv', sep=',', header=0)
# bankdf = bankdf.head(500)


# # getting the user age unique
# print(bankdf['Job'].unique().tolist())
# unique_age_people = bankdf['Age'].unique().tolist()
# print(unique_age_people.sort())


jobs = {
    'admin.': 1,
    'blue-collar': 2,
    'entrepreneur': 3,
    'housemaid': 4,
    'management':5,
    'retired': 6,
    'self-employed': 7,
    'services': 8,
    'student': 9,
    'technician': 10,
    'unemployed': 11,
    'unknown': 12
}


colormap = {
        1: 'red',
        2: 'green',
        3: 'orange',
        4: 'lightblue',
        5: 'chocolate',
        6: 'darkolivegreen',
        7: 'grey',
        8: 'purple',
        9: 'blue',
        10: 'orangered',
        11: 'yellow',
        12: 'pink'
}


colors = [colormap[x] for x in bankdf["Job"].values]
print(colors)

p = figure(title = "Relationship job holders and No of time contacted")
p.xaxis.axis_label = 'No of Job Holders'
p.yaxis.axis_label = 'Number of Time Contacts'

p.circle(flowers["petal_length"], flowers["petal_width"],
         color=colors, fill_alpha=0.4, size=15)

output_file("jitter.html", title="iris.py example")

show(p)