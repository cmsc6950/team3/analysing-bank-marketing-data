from math import pi

import cleandata
import pandas as pd

from bokeh.io import output_file, show
from bokeh.palettes import Category20c
from bokeh.plotting import figure
from bokeh.transform import cumsum


def pieChart():

    # Output filename: pieChart.html
    output_file("pieChart.html")
    # clean the dataset
    cleandata.cleandata()

    bankdf = pd.read_csv('ProcessedBankData.csv', sep=',', header=0)
    #print("Shape of Original data", bankdf.shape)

    #print(bankdf)
    ## getting the first 2000 data
    #bankdf = bankdf.head()

    print("Shape of Original data", bankdf.shape)
    print(bankdf.info())

    # getting the user age unique
    #print(bankdf['Age'].unique().tolist())
    unique_age_people = bankdf['Age'].unique().tolist()
    #print(unique_age_people.sort())


    ranges = {}

    # setting the starting range from 15 to 20
    # and increasing it by 5 every unique age
    range_first = 15
    range_last = 20
    for age in unique_age_people:
        total_people = 0
        if (range_first < 100 and range_last < 100):
            for index, row in bankdf.iterrows():
                if(row['Age'] >= range_first and row['Age'] < range_last and row['Housing loan'] == 1 and row['Term deposit subscription'] == 1):
                    total_people += 1
            range_first += 5
            range_last += 5
        ranges["Age Between: " + str(range_first) + " - " + str(range_last)] = total_people



    print(ranges)


    data = pd.Series(ranges).reset_index(name='value').rename(columns={'index': 'country'})
    data['angle'] = data['value'] / data['value'].sum() * 2 * pi
    data['color'] = Category20c[len(ranges)]

    p = figure(plot_height=350, title="Relationship Between Age Intervals, House Loan and Term Deposit", toolbar_location=None,
               tools="hover", tooltips="@country: @value", x_range=(-0.5, 1.0))

    p.wedge(x=0, y=1, radius=0.4,
            start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
            line_color="white", fill_color='color', legend='country', source=data)

    p.axis.axis_label = None
    p.axis.visible = False
    p.grid.grid_line_color = None

    show(p)

pieChart()