import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import style
plots = pd.read_csv("ProcessedBankData.csv")



fig, ax = plt.subplots(2,2)  # a figure with a 1x2 grid of Axes
fig.suptitle('Dot plots- Term deposit subsciption on X axis')
ax[0,0].plot(plots["Term deposit subscription"],plots["Avg yearly balance"], 'ro')
ax[0,0].set_ylabel('Avg yearly balance')
ax[0,1].plot(plots["Term deposit subscription"],plots["Age"], 'ro')
ax[0,1].set_ylabel('Age')
ax[1,0].plot(plots["Term deposit subscription"],plots["Job"], 'ro')
ax[1,0].set_ylabel('Job')
ax[1,0].set_ylim(-4000,10000)
ax[1,1].plot(plots["Term deposit subscription"],plots["# of times customer was contacted"], 'ro')
ax[1,1].set_ylabel('No of times customer was contacted')
ax[1,1].set_ylim(0,35)
fig.align_ylabels(ax)

# plt.boxplot(plots["Job"])

# plt.boxplot(plots["Term deposit subscription"],plots["Age"])
# plt.xlabel('Age')
# plt.xlabel('Term deposit')
plt.show()
fig.savefig('Plot_Dot.png')








# plt.plot(plots["Term deposit subscription"],plots["Avg yearly balance"], 'ro')
# plt.ylabel('Avg yearly balance')
# plt.xlabel('Term deposit')
# plt.xlim(-1,5)



# # plt.axis([0, 6, 0, 20])
# plt.show()


